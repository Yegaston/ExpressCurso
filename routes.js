const express = require('express');
const router = express.Router();

//rutas

router.get('/', (req, res) => {
    res.render('index.ejs');
})
// req es el requirimiento y res la respuesta.
// res.end es la respuesta que se da al final.

router.get('/login', (req, res) => {
    res.render('login.ejs');
});
//Render ingresa html 


module.exports = router;