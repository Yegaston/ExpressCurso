const express = require('express');
const app = express();
const morgan = require('morgan');

// requirimiento de rutas

var routes = require('./routes.js');
const routesApi = require ('./routes-api')

// setting

app.set('appName', 'Mi primer server');
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs'); // Template


// Middlewares

app.use(morgan('dev'));
app.use(morgan('short'));
app.use(morgan('combined'));


// app.use((req, res, next) => {
//     console.log('request url: ' + req.url);
//     next();
// });
// Muestra la ruta a la que quiere ingresar el user

// app.use((req, res, next) => {
//     console.log('Ha pasado por esta funcion');
//     next();
// });

// rutas

app.use(routes);
app.use('/api',routesApi);
app.get('*', (req, res) => {
    res.end('Error 404: Not Found');
});
// * significa todas las rutas que no esten declaradas.


app.listen(3000, () => {
    console.log("Server on in port 3000");
    console.log("Nombre de la app: " + app.get('appName'));
});
//app.listen dice el lugar donde escucha y la funcion 
// es el mensaje que se da una vez abierto el servidor.

